package controllers;

import java.util.Map;

import entity.UserPO;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import repository.UserRepository;

@Controller
public class UserController {

    private UserRepository userRepository;

    @GetMapping("/")
    public String mainPage () {
        return "login";
    }

    @GetMapping(value = "/account")
    public String checkAccount(@RequestParam("mail") String mail,
                              @RequestParam("password") String password,
                              Model model) {
        UserPO userPO = userRepository.findFirstByEmailAndPassword(mail, password);
        if (userPO != null && userPO.isActive()) {
            model.addAttribute("id", userPO.getId());
            model.addAttribute("name", userPO.getFirstName());
            model.addAttribute("lastName", userPO.getLastName());
            model.addAttribute("email", userPO.getLastName());
            model.addAttribute("birthday", userPO.getBirthday());
            return "index";
        } else {
            return "failed_login";
        }
    }

    @GetMapping(value = "/redirectOnRegistration")
    public String redirectOnRegistration() {
        return "registration";
    }

    @PostMapping(value = "/createAccount", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String registration(@RequestBody MultiValueMap multiValueMap, Model model) {
        Map singleValueMap = multiValueMap.toSingleValueMap();
        UserPO newUser = UserPO.builder()
                .firstName((String) singleValueMap.get("firstName"))
                .lastName((String) singleValueMap.get("lastName"))
                .email((String) singleValueMap.get("mail"))
                .password((String) singleValueMap.get("password"))
                .birthday(DateTime.parse((String) singleValueMap.get("birthday")))
                .active(Boolean.TRUE)
                .build();
        UserPO save = userRepository.save(newUser);
        model.addAttribute("id", save.getId());
        model.addAttribute("name", save.getFirstName());
        return "index";
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
