package repository;

import entity.UserPO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends CrudRepository<UserPO, Long> {

    @Transactional(rollbackFor = Exception.class)
    UserPO findFirstByEmailAndPassword(String email, String password);

}
