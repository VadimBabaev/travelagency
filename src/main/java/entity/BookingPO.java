package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

//Table name was changed (order -> booking), because 'order' is reserved sql word https://gyazo.com/3222694a4cb2ab94e679bcffeb101697
@Entity
@Table(name = "booking")
@Setter
@Getter
public class BookingPO {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserPO user;

    @ManyToOne
    @JoinColumn(name = "tour_id")
    private TourPO tour;

    @Column
    private boolean confirmed;

    @Column(name = "time_key")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime timeKey;

}
