$(document).ready(function () {
    $('#tryToLogin').submit(function (event) {
        var mail = $('#mail').val();
        var password = $('#password').val();
        var data = 'mail='
            + encodeURIComponent(mail)
            + '&amp;password='
            + encodeURIComponent(password);
        $.ajax({
            url: $("#loginForm").attr("action"),
            data: data,
            type: "GET",

            success: function (response) {
                alert(response);
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
        return false;
    });
});

$(document).ready(function () {
    $('#goToRegistration').submit(function (event) {
        $.ajax({
            url: $("#toRegistrationForm").attr("action"),
            type: "GET",
            success: function (response) {
                alert(response);
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
        return false;
    });
});



