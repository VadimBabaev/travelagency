$('#newAccount').submit(function (event) {

    var newAccount = {
        firstName: document.getElementById("firstNameReg").value,
        lastName: document.getElementById("lastNameReg").value,
        email: document.getElementById("mailReg").value,
        password: document.getElementById("passwordReg").value,
        birthday: document.getElementById("birthdayReg").value
    };

    $.ajax({
        url: $("#registryForm").attr("action"),
        data: JSON.stringify(newAccount),
        dataType: 'json',
        contentType: 'application/json',
        type: "POST",

        success: function (response) {
            alert(response);
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
    return false;
});

